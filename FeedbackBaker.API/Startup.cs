using FeedbackBaker.API.Repository;
using FeedbackBaker.API.Repository.Abstract;
using FeedbackBaker.API.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FeedbackBaker.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            RegisterRepositories(ref services);
            RegisterServices(ref services);

            services.AddCors(options =>
            {
                options.AddPolicy(Constants.CorsPolicyName,
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:3000", "http://localhost:3001");
                        builder.WithMethods("GET", "POST", "OPTIONS");
                        builder.AllowAnyHeader();
                    });
            });

            services.AddControllers();

            services.AddDbContext<FeedbackBakerDbContext>(options =>
                options.UseSqlite("Filename=FeedbackBakerDatabase.db"));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors(Constants.CorsPolicyName);

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        private static void RegisterServices(ref IServiceCollection services)
        {
            services.AddTransient<IFeedbackService, FeedbackService>();
        }

        private static void RegisterRepositories(ref IServiceCollection services)
        {
            services.AddTransient<IMeetingsRepository, MeetingsRepository>();
            services.AddTransient<IVotesRepository, VotesRepository>();
            services.AddTransient<IParticipantsRepository, ParticipantsRepository>();
            services.AddTransient<IQuestionsRepository, QuestionRepository>();
        }
    }
}
﻿using FeedbackBaker.API.Models;
using FeedbackBaker.API.Repository.Abstract;

namespace FeedbackBaker.API.Repository
{
    public class VotesRepository : Repository<Vote>, IVotesRepository
    {
        public VotesRepository(FeedbackBakerDbContext dbContext) : base(dbContext)
        {
        }
    }
}

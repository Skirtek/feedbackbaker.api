﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FeedbackBaker.API.Models;
using FeedbackBaker.API.Repository.Abstract;

namespace FeedbackBaker.API.Repository
{
    public class QuestionRepository : Repository<Question>, IQuestionsRepository
    {
        private readonly FeedbackBakerDbContext _dbContext;

        public QuestionRepository(FeedbackBakerDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Question>> GetQuestions(string meetingId) =>
            await Task.Run(() => GetAll().Where(x => x.MeetingId == meetingId));

        public override async Task<bool> DeleteAll(string id)
        {
            try
            {
                var meetings = GetAll().Where(x => x.MeetingId == id).ToList();

                if (!meetings.Any())
                {
                    return true;
                }

                Entities.RemoveRange(meetings);
                await _dbContext.SaveChangesAsync();

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}

﻿using System.Threading.Tasks;
using FeedbackBaker.API.Models;

namespace FeedbackBaker.API.Repository.Abstract
{
    public interface IMeetingsRepository : IRepository<Meeting>
    {
        Task<Meeting> GetMeeting(string meetingId);
    }
}
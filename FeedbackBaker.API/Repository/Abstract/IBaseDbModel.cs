﻿namespace FeedbackBaker.API.Repository.Abstract
{
    public interface IBaseDbModel
    {
        int Id { get; set; }
    }
}
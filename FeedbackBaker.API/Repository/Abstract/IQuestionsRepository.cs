﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FeedbackBaker.API.Models;

namespace FeedbackBaker.API.Repository.Abstract
{
    public interface IQuestionsRepository : IRepository<Question>
    {
        Task<IEnumerable<Question>> GetQuestions(string meetingId);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FeedbackBaker.API.Repository.Abstract
{
    public interface IRepository<T>
    {
        Task<bool> Create(T item);

        Task<bool> CreateBatch(IEnumerable<T> items);

        Task<bool> DeleteAll(string id);

        IQueryable<T> GetAll();

        IQueryable<T> GetAll(Expression<Func<T, bool>> predicate);

        bool Update(T item);
    }
}
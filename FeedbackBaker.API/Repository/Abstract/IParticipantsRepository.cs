﻿using FeedbackBaker.API.Models;

namespace FeedbackBaker.API.Repository.Abstract
{
    public interface IParticipantsRepository : IRepository<Participant>
    {
        
    }
}
﻿using FeedbackBaker.API.Models;

namespace FeedbackBaker.API.Repository.Abstract
{
    public interface IVotesRepository : IRepository<Vote>
    {
        
    }
}
﻿using FeedbackBaker.API.Models;
using FeedbackBaker.API.Repository.Abstract;

namespace FeedbackBaker.API.Repository
{
    public class ParticipantsRepository : Repository<Participant>, IParticipantsRepository
    {
        public ParticipantsRepository(FeedbackBakerDbContext dbContext) : base(dbContext)
        {
        }
    }
}

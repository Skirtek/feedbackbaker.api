﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using FeedbackBaker.API.Repository.Abstract;
using Microsoft.EntityFrameworkCore;

namespace FeedbackBaker.API.Repository
{
    public class Repository<T> : IRepository<T> where T : class, IBaseDbModel, new()
    {
        private readonly FeedbackBakerDbContext _dbContext;

        public Repository(FeedbackBakerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        protected DbSet<T> Entities => _dbContext.Set<T>();

        public async Task<bool> Create(T item)
        {
            try
            {
                await Entities.AddAsync(item);
                await _dbContext.SaveChangesAsync();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> CreateBatch(IEnumerable<T> items)
        {
            try
            {
                foreach (var item in items)
                {
                    await Entities.AddAsync(item);
                }

                await _dbContext.SaveChangesAsync();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public virtual Task<bool> DeleteAll(string id)
        {
            return Task.FromResult(true);
        }

        public virtual IQueryable<T> GetAll()
        {
            try
            {
                return Entities.AsNoTracking();
            }
            catch
            {
                return null;
            }
        }

        public virtual IQueryable<T> GetAll(Expression<Func<T, bool>> predicate)
        {
            try
            {
                return Entities.Where(predicate);
            }
            catch
            {

                return null;
            }
        }

        public virtual bool Update(T item)
        {
            try
            {
                var entity = Entities.Find(item.Id);

                if (entity == null)
                {
                    return false;
                }

                _dbContext.Entry(entity).CurrentValues.SetValues(item);

                _dbContext.SaveChanges();

                _dbContext.Entry(entity).State = EntityState.Detached;

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}

﻿using System.Threading.Tasks;
using FeedbackBaker.API.Models;
using FeedbackBaker.API.Repository.Abstract;
using Microsoft.EntityFrameworkCore;

namespace FeedbackBaker.API.Repository
{
    public class MeetingsRepository : Repository<Meeting>, IMeetingsRepository
    {
        private readonly FeedbackBakerDbContext _dbContext;

        public MeetingsRepository(FeedbackBakerDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<Meeting> GetMeeting(string meetingId) =>
            _dbContext.Meetings.FirstOrDefaultAsync(x => x.MeetingId == meetingId);
    }
}

﻿using FeedbackBaker.API.Models;
using Microsoft.EntityFrameworkCore;

namespace FeedbackBaker.API.Repository
{
    public class FeedbackBakerDbContext : DbContext
    {
        public DbSet<Participant> Participants { get; set; }

        public DbSet<Question> Questions { get; set; }

        public DbSet<Meeting> Meetings { get; set; }

        public DbSet<Vote> Votes { get; set; }

        public FeedbackBakerDbContext(DbContextOptions<FeedbackBakerDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Participant>().ToTable("Participants", "FeedbackBaker");
            modelBuilder.Entity<Question>().ToTable("Questions", "FeedbackBaker");
            modelBuilder.Entity<Meeting>().ToTable("Meetings", "FeedbackBaker");
            modelBuilder.Entity<Vote>().ToTable("Votes", "FeedbackBaker");

            modelBuilder.Entity<Meeting>(entity =>
            {
                entity.Property(e => e.Date).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}

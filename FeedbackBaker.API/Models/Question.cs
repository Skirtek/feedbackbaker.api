﻿using System.ComponentModel.DataAnnotations;
using FeedbackBaker.API.Repository.Abstract;
using SQLite;

namespace FeedbackBaker.API.Models
{
    public class Question : IBaseDbModel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string MeetingId { get; set; }

        public int Order { get; set; }

        [Required]
        public string Content { get; set; }
    }
}

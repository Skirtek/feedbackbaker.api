﻿using System;
using System.ComponentModel.DataAnnotations;
using FeedbackBaker.API.Repository.Abstract;
using SQLite;

namespace FeedbackBaker.API.Models
{
    public class Meeting : IBaseDbModel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Required]
        public string MeetingId { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string CreatorId { get; set; }

        [Required]
        public DateTime Date { get; set; }
    }
}

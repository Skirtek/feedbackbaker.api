﻿using FeedbackBaker.API.Repository.Abstract;
using SQLite;

namespace FeedbackBaker.API.Models
{
    public class Participant : IBaseDbModel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}

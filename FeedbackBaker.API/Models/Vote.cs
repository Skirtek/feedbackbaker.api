﻿using System.ComponentModel.DataAnnotations;
using FeedbackBaker.API.Repository.Abstract;
using SQLite;

namespace FeedbackBaker.API.Models
{
    public class Vote : IBaseDbModel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        
        public int ParticipantId { get; set; }

        public int QuestionId { get; set; }

        [Required]
        public string Content { get; set; }
    }
}

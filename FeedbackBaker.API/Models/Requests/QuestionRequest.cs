﻿namespace FeedbackBaker.API.Models.Requests
{
    public class QuestionRequest
    {
        public string Content { get; set; }

        public int Order { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;
using ecom.Utilities;

namespace FeedbackBaker.API.Models.Requests
{
    public class VoteRequest
    {
        public int ParticipantId { get; set; }

        public int QuestionId { get; set; }

        [Required]
        public string Content { get; set; }

        public bool IsValid => RequestsHelper.HasRequiredFields(this);
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using ecom.Utilities;

namespace FeedbackBaker.API.Models.Requests
{
    public class MeetingRequest
    {
        [Required]
        public string Title { get; set; }

        public DateTime Date { get; set; }

        [Required]
        public string CreatorId { get; set; }

        public bool IsValid => RequestsHelper.HasRequiredFields(this);
    }
}
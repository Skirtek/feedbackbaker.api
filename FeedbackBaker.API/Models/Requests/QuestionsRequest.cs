﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ecom.Utilities;

namespace FeedbackBaker.API.Models.Requests
{
    public class QuestionsRequest
    {
        [Required]
        public string MeetingId { get; set; }

        public List<QuestionRequest> Questions { get; set; }

        public bool IsValid => RequestsHelper.HasRequiredFields(this);
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ecom.Common.Models;
using FeedbackBaker.API.Models;
using FeedbackBaker.API.Models.Requests;

namespace FeedbackBaker.API.Service
{
    public interface IFeedbackService
    {
        Task<Response> CreateMeeting(MeetingRequest request);

        Task<Response> AddQuestions(QuestionsRequest request);

        Task<Response> AddVote(VoteRequest request);

        Task<Response<Meeting>> GetMeeting(string meetingId);

        Task<Response<IEnumerable<Question>>> GetQuestions(string meetingId);

        Task<Response<List<Vote>>> GetVotes(string meetingId);
    }
}
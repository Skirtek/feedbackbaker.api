﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ecom.Common.Models;
using FeedbackBaker.API.Models;
using FeedbackBaker.API.Models.Requests;
using FeedbackBaker.API.Repository.Abstract;
using Microsoft.Extensions.Logging;

namespace FeedbackBaker.API.Service
{
    public class FeedbackService : IFeedbackService
    {
        private readonly ILogger<FeedbackService> _logger;
        private readonly IMeetingsRepository _meetingsRepository;
        private readonly IQuestionsRepository _questionsRepository;
        private readonly IVotesRepository _votesRepository;

        public FeedbackService(
            ILogger<FeedbackService> logger,
            IMeetingsRepository meetingsRepository,
            IQuestionsRepository questionRepository,
            IVotesRepository votesRepository)
        {
            _logger = logger;
            _meetingsRepository = meetingsRepository;
            _questionsRepository = questionRepository;
            _votesRepository = votesRepository;
        }

        public async Task<Response> CreateMeeting(MeetingRequest request)
        {
            bool result = await _meetingsRepository.Create(new Meeting
            {
                Date = request.Date,
                Title = request.Title,
                CreatorId = request.CreatorId
            });

            return result ? Response.Succeeded() : Response.Failed();
        }

        public async Task<Response> AddQuestions(QuestionsRequest request)
        {
            var deleteResult = await _questionsRepository.DeleteAll(request.MeetingId);

            if (!deleteResult)
            {
                return Response.Failed();
            }

            var result = await _questionsRepository.CreateBatch(request.Questions.Select(x => new Question
            {
                Content = x.Content,
                MeetingId = request.MeetingId,
                Order = x.Order
            }));

            return result ? Response.Succeeded() : Response.Failed();
        }

        public async Task<Response> AddVote(VoteRequest request)
        {
            var result = await _votesRepository.Create(new Vote
            {
                Content = request.Content,
                ParticipantId = request.ParticipantId,
                QuestionId = request.QuestionId
            });

            return result ? Response.Succeeded() : Response.Failed();
        }

        public async Task<Response<Meeting>> GetMeeting(string meetingId)
        {
            var result = await _meetingsRepository.GetMeeting(meetingId);

            return result != null ? Response<Meeting>.Succeeded(result) : Response<Meeting>.Failed();
        }

        public async Task<Response<IEnumerable<Question>>> GetQuestions(string meetingId)
        {
            var result = await _questionsRepository.GetQuestions(meetingId);

            return result != null ? Response<IEnumerable<Question>>.Succeeded(result.ToList()) : Response<IEnumerable<Question>>.Failed();
        }

        public async Task<Response<List<Vote>>> GetVotes(string meetingId)
        {
            try
            {
                var questions = await _questionsRepository.GetQuestions(meetingId);

                if (questions == null)
                {
                    return Response<List<Vote>>.Failed();
                }

                var currentMeetingsQuestions = questions.Where(x => x.MeetingId == meetingId);

                var votes = new List<Vote>();

                foreach (var question in currentMeetingsQuestions)
                {
                    var vote = _votesRepository.GetAll(x => x.QuestionId == question.Id).FirstOrDefault();

                    if (vote != null)
                    {
                        votes.Add(vote);
                    }
                }

                return Response<List<Vote>>.Succeeded(votes);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex.ToString());
                return Response<List<Vote>>.Failed();
            }
        }
    }
}
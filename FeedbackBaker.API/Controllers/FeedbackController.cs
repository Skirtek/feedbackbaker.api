﻿using System.Threading.Tasks;
using FeedbackBaker.API.Models.Requests;
using FeedbackBaker.API.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FeedbackBaker.API.Controllers
{
    [ApiController]
    [Route("/api/feedback/[action]")]
    public class FeedbackController : ControllerBase
    {
        private readonly IFeedbackService _feedbackService;

        public FeedbackController(IFeedbackService feedbackService)
        {
            _feedbackService = feedbackService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateMeeting([FromBody] MeetingRequest request)
        {
            if (request == null || !request.IsValid)
            {
                return BadRequest();
            }

            var getResult = await _feedbackService.CreateMeeting(request);

            return getResult.IsSuccess
                ? Ok(getResult)
                : new StatusCodeResult(StatusCodes.Status500InternalServerError);
        }

        [HttpPost]
        public async Task<IActionResult> AddQuestions([FromBody] QuestionsRequest request)
        {
            if (request == null || !request.IsValid)
            {
                return BadRequest();
            }

            var getResult = await _feedbackService.AddQuestions(request);

            return getResult.IsSuccess
                ? Ok(getResult)
                : new StatusCodeResult(StatusCodes.Status500InternalServerError);
        }

        [HttpPost]
        public async Task<IActionResult> AddVote([FromBody] VoteRequest request)
        {
            if (request == null || !request.IsValid)
            {
                return BadRequest();
            }

            var getResult = await _feedbackService.AddVote(request);

            return getResult.IsSuccess
                ? Ok(getResult)
                : new StatusCodeResult(StatusCodes.Status500InternalServerError);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetMeeting(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return BadRequest();
            }

            var getResult = await _feedbackService.GetMeeting(id);

            return getResult.IsSuccess
                ? Ok(getResult)
                : new StatusCodeResult(StatusCodes.Status500InternalServerError);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetQuestions(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return BadRequest();
            }

            var getResult = await _feedbackService.GetQuestions(id);

            return getResult.IsSuccess
                ? Ok(getResult)
                : new StatusCodeResult(StatusCodes.Status500InternalServerError);
        }

        [HttpGet("{meetingId}")]
        public async Task<IActionResult> GetVotes(string meetingId)
        {
            if (string.IsNullOrWhiteSpace(meetingId))
            {
                return BadRequest();
            }

            var getResult = await _feedbackService.GetVotes(meetingId);

            return getResult.IsSuccess
                ? Ok(getResult)
                : new StatusCodeResult(StatusCodes.Status500InternalServerError);
        }
    }
}
